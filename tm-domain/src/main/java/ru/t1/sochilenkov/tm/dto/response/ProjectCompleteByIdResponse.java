package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Project;

@NoArgsConstructor
public final class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
