package ru.t1.sochilenkov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.Project;

@NoArgsConstructor
public final class ProjectClearResponse extends AbstractProjectResponse {

    public ProjectClearResponse(@Nullable final Project project) {
        super(project);
    }

}
