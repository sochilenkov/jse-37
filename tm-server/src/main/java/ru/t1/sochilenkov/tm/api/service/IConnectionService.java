package ru.t1.sochilenkov.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
