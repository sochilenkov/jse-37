package ru.t1.sochilenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.repository.ISessionRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.model.Session;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository, @NotNull IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
    }

}
