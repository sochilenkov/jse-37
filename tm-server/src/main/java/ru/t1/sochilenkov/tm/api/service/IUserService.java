package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception ;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception ;

    @NotNull
    User add(@Nullable User user) throws Exception ;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findById(@Nullable String Id) throws Exception;

    @Nullable
    User findByLogin(@Nullable String Login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @NotNull
    User remove(@Nullable User model) throws Exception;

    @Nullable
    User removeById(@Nullable String Id) throws Exception;

    @NotNull
    User removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    Boolean isLoginExist(@Nullable String login) throws Exception;

    Boolean isEmailExist(@Nullable String email) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
