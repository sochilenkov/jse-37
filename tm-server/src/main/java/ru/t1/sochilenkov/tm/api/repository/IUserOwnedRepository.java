package ru.t1.sochilenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M add(@Nullable String userId, @NotNull M model) throws Exception;

    boolean existsById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    @Nullable
    M remove(@NotNull String userId, @NotNull M model) throws Exception;

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

}
