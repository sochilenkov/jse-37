package ru.t1.sochilenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.endpoint.IUserEndpoint;
import ru.t1.sochilenkov.tm.api.service.IAuthService;
import ru.t1.sochilenkov.tm.api.service.IServiceLocator;
import ru.t1.sochilenkov.tm.api.service.IUserService;
import ru.t1.sochilenkov.tm.dto.request.*;
import ru.t1.sochilenkov.tm.dto.response.*;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.model.Session;
import ru.t1.sochilenkov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.sochilenkov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) throws Exception {
        @NotNull Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) throws Exception {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @Nullable final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) throws Exception {
        @NotNull Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final User user = getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
        return new UserUpdateProfileResponse(user);
    }

}
