package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.ITaskRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.Task;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.*;

import static ru.t1.sochilenkov.tm.constant.ProjectConstant.USER_ID_1;
import static ru.t1.sochilenkov.tm.constant.TaskConstant.*;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private ITaskRepository repository;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private Project project;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        repository = new TaskRepository();
        connection = connectionService.getConnection();
        repository.setRepositoryConnection(connection);
        connection.setAutoCommit(true);
        taskList = new ArrayList<>();
        project = new Project();
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            task.setProjectId(project.getId());
            repository.add(USER_ID_1, task);
            task.setUserId(USER_ID_1);
            taskList.add(task);
        }
        for (int i = 1; i <= INIT_COUNT_TASKS; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task_" + i);
            task.setDescription("Description_" + i);
            repository.add(USER_ID_2, task);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
    }

    @After
    public void closeConnection() throws Exception {
        repository.clear();
        connection.close();
    }

    @Test
    public void testAddTaskPositive() throws Exception {
        Task task = new Task();
        task.setName("TaskAddTest");
        task.setDescription("TaskAddTest desc");
        Assert.assertNull(repository.add(NULLABLE_USER_ID, task));
        Assert.assertNotNull(repository.add(USER_ID_1, task));
    }

    @Test
    public void testClear() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        repository.clear(USER_ID_1);
        Assert.assertEquals(0, repository.getSize(USER_ID_1));

        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        repository.clear(USER_ID_2);
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testClearAll() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() throws Exception {
        Assert.assertNull(repository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            final Task foundTaskWOUser = repository.findOneById(task.getId());
            final Task foundTask = repository.findOneById(task.getUserId(), task.getId());
            Assert.assertNotNull(foundTaskWOUser);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(task.getId(), foundTaskWOUser.getId());
            Assert.assertEquals(task.getId(), foundTask.getId());
        }
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertFalse(repository.existsById(USER_ID_1, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(USER_ID_2, UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            Assert.assertTrue(repository.existsById(task.getId()));
            Assert.assertTrue(repository.existsById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void testFindByIndex() throws Exception {
        Assert.assertNull(repository.findOneByIndex(USER_ID_1, 9999));
        Assert.assertNull(repository.findOneByIndex(USER_ID_2, 9999));
        Assert.assertNull(repository.findOneByIndex(9999));
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            final Task foundTaskWOUser = repository.findOneByIndex(taskList.indexOf(taskList.get(i)) + 1);
            Assert.assertNotNull(foundTaskWOUser);
            Assert.assertEquals(taskList.get(i).getId(), foundTaskWOUser.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_ID_1, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i).getId(), foundTask.getId());
        }
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            final Task foundTask = repository.findOneByIndex(USER_ID_2, i + 1);
            Assert.assertNotNull(foundTask);
            Assert.assertEquals(taskList.get(i + 5).getId(), foundTask.getId());
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<Task> tasks = repository.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_ID_1);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertEquals(tasks.get(i).getId(), taskList.get(i).getId());
        }
        tasks = repository.findAll(USER_ID_2);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertEquals(tasks.get(i-5).getId(), taskList.get(i).getId());
        }
    }

    @Test
    public void testFindAllComparator() throws Exception {
        List<Task> tasks = repository.findAll(TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
        for (final Task task : taskList) {
            Assert.assertNotNull(
                    tasks.stream()
                            .filter(m -> task.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null));
        }
        tasks = repository.findAll(USER_ID_1, TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
        tasks = repository.findAll(USER_ID_2, TASK_COMPARATOR);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
        for (final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_2)) {
                Assert.assertNotNull(
                        tasks.stream()
                                .filter(m -> task.getId().equals(m.getId()))
                                .filter(m -> task.getUserId().equals(m.getUserId()))
                                .findFirst()
                                .orElse(null));
            }
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdWOUserNegative() throws Exception {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNotNull(repository.removeById(taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveById() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeById(USER_ID_1, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_1, taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_1, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNull(repository.removeById(USER_ID_2, UUID.randomUUID().toString()));
            Assert.assertNotNull(repository.removeById(USER_ID_2, taskList.get(i).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_2, taskList.get(i).getId()));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexWOUserNegative() throws Exception {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (int i = 0; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNotNull(repository.removeByIndex(1));
            Assert.assertEquals(INIT_COUNT_TASKS * 2 - i - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_1, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_1, 1));
            Assert.assertEquals(INIT_COUNT_TASKS - i - 1, repository.getSize(USER_ID_1));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNull(repository.removeByIndex(USER_ID_2, 9999));
            Assert.assertNotNull(repository.removeByIndex(USER_ID_2, 1));
            Assert.assertEquals(INIT_COUNT_TASKS - i - 1, repository.getSize(USER_ID_2));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveWOUserPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS * 2, repository.getSize());
        for (final Task task : taskList) {
            Assert.assertNotNull(repository.remove(task));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemove() throws Exception {
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_1));
        for (int i = 0; i < INIT_COUNT_TASKS; i++) {
            Assert.assertNotNull(repository.remove(USER_ID_1, taskList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_1));
        Assert.assertEquals(INIT_COUNT_TASKS, repository.getSize(USER_ID_2));
        for (int i = 5; i < INIT_COUNT_TASKS * 2; i++) {
            Assert.assertNotNull(repository.remove(USER_ID_2, taskList.get(i)));
        }
        Assert.assertEquals(0, repository.getSize(USER_ID_2));
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertNotNull(repository.set(Arrays.asList(new Task(), new Task(), new Task())));
        Assert.assertEquals(3, repository.getSize());
    }

    @Test
    public void testTaskFindByProjectId() throws Exception {
        List<Task> tasks = repository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(INIT_COUNT_TASKS, tasks.size());
    }

}
