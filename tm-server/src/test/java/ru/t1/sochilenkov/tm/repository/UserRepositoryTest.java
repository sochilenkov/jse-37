package ru.t1.sochilenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.*;

import static ru.t1.sochilenkov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private IUserRepository repository;

    @NotNull
    private List<User> userList;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
    }

    @Before
    public void init() throws Exception {
        repository = new UserRepository();
        connection = connectionService.getConnection();
        repository.setRepositoryConnection(connection);
        connection.setAutoCommit(true);
        userList = new ArrayList<>();
        repository.clear();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            repository.add(user);
            userList.add(user);
        }
    }

    @After
    public void closeConnection() throws Exception {
        repository.clear();
        connection.close();
    }

    @Test
    public void testAddUserPositive() throws Exception {
        User user = new User();
        user.setLogin("UserAddTest");
        Assert.assertNotNull(repository.add(user));
    }

    @Test
    public void testClear() throws Exception {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() throws Exception {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final User user : userList) {
            final User foundUser = repository.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user.getId(), foundUser.getId());
        }
    }

    @Test
    public void testExistsById() throws Exception {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final User user : userList) {
            Assert.assertTrue(repository.existsById(user.getId()));
        }
    }

    @Test
    public void testFindByIndex() throws Exception {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (final User user : userList) {
            final User foundUser = repository.findOneByIndex(userList.indexOf(user) + 1);
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user.getId(), foundUser.getId());
        }
    }

    @Test
    public void testFindAll() throws Exception {
        List<User> users = repository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final User user : userList) {
            Assert.assertEquals(user.getId(), users.get(userList.indexOf(user)).getId());
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIdNegative() throws Exception {
        repository.removeById(UUID.randomUUID().toString());
    }

    @Test
    public void testRemoveByIdPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.removeById(user.getId()));
            Assert.assertNull(repository.findOneById(user.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveByIndexNegative() throws Exception {
        repository.removeByIndex(9999);
    }

    @Test
    public void testRemoveByIndexPositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.removeByIndex(1));
            Assert.assertEquals(INIT_COUNT_USERS - userList.indexOf(user) - 1, repository.getSize());
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemovePositive() throws Exception {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final User user : userList) {
            Assert.assertNotNull(repository.remove(user));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertNotNull(repository.set(Arrays.asList(new User(), new User(), new User())));
        Assert.assertEquals(3, repository.getSize());
    }

    @Test
    public void testExistsLogin() throws Exception {
        Assert.assertFalse(repository.isLoginExist(""));
        for (final User user : userList) {
            Assert.assertTrue(repository.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testFindByLogin() throws Exception {
        Assert.assertNull(repository.findByLogin(""));
        for (final User user : userList) {
            Assert.assertNotNull(repository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testExistsEmail() throws Exception {
        Assert.assertFalse(repository.isEmailExist(""));
        for (final User user : userList) {
            Assert.assertTrue(repository.isEmailExist(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmail() throws Exception {
        Assert.assertNull(repository.findByEmail(""));
        for (final User user : userList) {
            Assert.assertNotNull(repository.findByEmail(user.getEmail()));
        }
    }

}
