package ru.t1.sochilenkov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.model.Project;

import java.util.Comparator;
import java.util.UUID;

public interface ProjectConstant {

    int INIT_COUNT_PROJECTS = 5;

    @NotNull
    String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    String USER_ID_2 = UUID.randomUUID().toString();

    @Nullable
    Project NULLABLE_PROJECT = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_PROJECT_ID = null;

    @NotNull
    String EMPTY_PROJECT_ID = "";

    @NotNull
    Sort CREATED_SORT = Sort.BY_CREATED;

    @Nullable
    Sort NULLABLE_SORT = null;

    @NotNull
    Comparator<Project> PROJECT_COMPARATOR = CREATED_SORT.getComparator();

    @Nullable
    Comparator<Project> NULLABLE_COMPARATOR = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;


}
